using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NexusHealth : MonoBehaviour, IDamageable
{
    public Health Healthbar;

    public int Health {get; private set;} = 100000;

    // Start is called before the first frame update
    void Start()
    {
        Healthbar.SetStartingHealth(this.Health);
    }

    public void RecieveDamage(int damage)
    {
        this.Health -= damage;
        Healthbar.Damage(damage);

        if (this.Health <= 0)
        {
            OnDeath();
        }
    }

    public void OnDeath()
    {
        ActionMediator.Instance.Notify(gameObject, "Death");
    }
}
