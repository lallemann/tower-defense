using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingStar : MonoBehaviour
{
    public float Speed = 10;
    public float AttackRange = 1;
    public int Power = 10;
    public int TimeUntilDestroy = 5;
    public GameObject Target;

    // Start is called before the first frame update
    void Start()
    { 
        Destroy(gameObject, TimeUntilDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        if (Target ?? false)
        {
            MoveTowardsTarget();

            if (IsInAttackRangeFromTarget())
            {
                CombatMediator.Instance.DealDamage(
                    Target.GetComponent<IDamageable>(),
                    Power
                );

                Destroy(gameObject, 0);
            }
        }
        else
        {
            Destroy(gameObject, 0);
        }
    }

    private bool IsInAttackRangeFromTarget()
    {
        return Vector3.Distance(transform.position, Target.transform.position) < AttackRange;
    }

    private void MoveTowardsTarget()
    {
        transform.position = Vector2.MoveTowards(transform.position, Target.transform.position, Speed * Time.deltaTime);
    }
}
