using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterJet : MonoBehaviour
{
    public float Speed = 30;
    public float AttackRange = .1f;
    public int Power = 25;
    public int TimeUntilDestroy = 10;
    public GameObject Target;
    public Vector3 TargetInitialPosition = new Vector3(0.0f, 0.0f, 0.0f);

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, TimeUntilDestroy);
    }

    // Update is called once per frame
    void Update() 
    {
        if (Target ?? false)
        {
            MoveTowardsTarget();

            if (IsInAttackRangeFromTarget())
            {
                CombatMediator.Instance.DealDamage(
                    Target.GetComponent<IDamageable>(),
                    Power
                );

                Destroy(gameObject, 0);
            }
        }
        else
        {
            Destroy(gameObject, 0);
        }
    }

    private bool IsInAttackRangeFromTarget()
    {
        return Vector3.Distance(transform.position, TargetInitialPosition) < AttackRange;
    }

    private void MoveTowardsTarget()
    {
        transform.position = Vector2.MoveTowards(transform.position, TargetInitialPosition, Speed * Time.deltaTime);
    }
}
