using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttack : MonoBehaviour, ITower
{
    public float AttackCooldown = .5f;
    private List<TowerModifier> _ModifiersList = new List<TowerModifier>();

    private GameObject _Target;
    public GameObject tower;

    public float Range = 20f;

    // Start is called before the first frame update
    void Start()
    {
        // this.RegisterTowerModifier(new HomingTowerModifier(this, tower));
        StartCoroutine("ShootRepeatedlyCoroutine");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator ShootRepeatedlyCoroutine()
    {
        for(;;)
        {
            if (IsInRangeFromTarget())
            {
                Shoot();
            }
            else 
            {
                SearchNewTarget();
            }
            yield return new WaitForSeconds(AttackCooldown);
        }
    }
    
    public void Shoot() 
    {
        _ModifiersList.ForEach(
            modifier => {
                modifier.SetTarget(_Target);
                modifier.Shoot();
            }
        );
    }

    public void RegisterTowerModifier(TowerModifier modifier)
    {
        _ModifiersList.Add(modifier);
    }

    void SearchNewTarget()
    {
        Debug.Log("SearchNewTarget");
        RadiusScannerTool rsTool = new RadiusScannerTool(gameObject, Range);
        rsTool.Targets.Add("Monster");
        
        _Target = rsTool.FirstTargetWithinRadius();
    }

    bool IsInRangeFromTarget()
    {
        return (_Target??false) && 
            (Vector3.Distance(transform.position, _Target.transform.position) < Range);
    }
}
