using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileTower : Tile
{
    public GameObject TowerPrefab;
    private GameObject _AssociatedTower;

    private void Awake() {
        _AssociatedTower = Object.Instantiate(TowerPrefab);
    }
}
