using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public float Speed = 10;
    public float AttackRange = .1f;
    public int ExplosionPower = 30;
    public int BurnPower = 5;
    public float TimeBetweenBurns = 0.4f;
    public int NumberOfBurns = 5;
    public int TimeUntilDestroy = 5;
    public GameObject Target;
    public Vector3 TargetInitialPosition = new Vector3(0.0f, 0.0f, 0.0f);

    // Start is called before the first frame update
    void Start()
    { 
        TargetInitialPosition = Target.transform.position;
        Destroy(gameObject, TimeUntilDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        if (Target ?? false)
        {
            MoveTowardsTarget();

            if (IsInAttackRangeFromTarget())
            {
                CombatMediator.Instance.DealDamage(
                    Target.GetComponent<IDamageable>(),
                    ExplosionPower
                );

                CombatMediator.Instance.DealDamageOverTime(
                    Target.GetComponent<IDamageable>(),
                    BurnPower,
                    TimeBetweenBurns,
                    NumberOfBurns
                );

                Destroy(gameObject, 0);
            }
        }
        else
        {
            Destroy(gameObject, 0);
        }
    }

    private bool IsInAttackRangeFromTarget()
    {
        return Vector3.Distance(transform.position, TargetInitialPosition) < AttackRange;
    }

    private void MoveTowardsTarget()
    {
        transform.position = Vector2.MoveTowards(transform.position, TargetInitialPosition, Speed * Time.deltaTime);
    }
}
