using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour, IDamageable
{

    private int _Health = 100;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecieveDamage(int damage)
    {
        this._Health = this._Health - damage;
        Debug.Log("Reçu " + damage + "dégats !!!");
    }
}
