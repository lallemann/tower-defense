using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatMediator : MonoBehaviour, IDamager
{

    private static CombatMediator _Instance;

    public static CombatMediator Instance
    {
        get
        {
            if (_Instance??false || _Instance == null)
            {
                _Instance = FindObjectOfType<CombatMediator>();
            }

            return _Instance;
        }
    }

    private int _TotalDamageDealt = 0;
    public int TotalDamageDealt
    {
        get => _TotalDamageDealt;
        private set
        {
            _TotalDamageDealt = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DealDamage(IDamageable target, int damage)
    {
        // if (target??false)
        // {
            target.RecieveDamage(damage);
        // }
    }

    public void DealDamageOverTime(IDamageable target, int damagePerHit, float pauseTime, int hits)
    {
        StartCoroutine(DealDamageCoroutine(target, damagePerHit, pauseTime, hits));
    }

    IEnumerator DealDamageCoroutine(IDamageable target, int damagePerHit, float pauseTime, int hits)
    {
        for(int i = 0; i < hits; i++)
        {
            // if (target??false)
            // {
                target.RecieveDamage(damagePerHit);
            // }
            yield return new WaitForSeconds(pauseTime);
        }
    }
}
