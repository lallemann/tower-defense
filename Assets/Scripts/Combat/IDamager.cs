using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamager
{
    void DealDamage(IDamageable target, int damage);    

    void DealDamageOverTime(IDamageable target, int damagePerHit, float pauseTime, int hits);
}
