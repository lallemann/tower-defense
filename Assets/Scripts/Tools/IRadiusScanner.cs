using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRadiusScanner
{
    void OnObjectDetected(GameObject scannedObject);
}