using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiusScannerTool
{

    /**
    
    */
    public List<string> Targets = new List<string>();

    public float Radius;

    private GameObject _Source;

    public RadiusScannerTool(GameObject source, float radius)
    {
        this._Source = source;
        this.Radius = radius;
    }

    public List<GameObject> ScanObjectsWithinRadius()
    {
        List<GameObject> returnList = new List<GameObject>();
        Collider[] colliders = Physics.OverlapSphere(_Source.transform.position, Radius);  
        
        foreach(Collider collider in colliders)
        {
            foreach (string stringClass in Targets)
            {
                
                if(collider.gameObject.GetComponent(stringClass) != null)
                {
                    returnList.Add(collider.gameObject);
                }
            }
            
        }

        return returnList; 
    }

    public GameObject FirstTargetWithinRadius()
    {
        Collider[] colliders = Physics.OverlapSphere(_Source.transform.position, Radius);

        // Debug.Log(_Source.transform.position);

        foreach(Collider collider in colliders)
        {
            // Debug.Log("collider: "+collider.gameObject);
            foreach (string targetTag in Targets)
            {
                // Debug.Log("Tag: "+collider.gameObject.tag);
                if(collider.gameObject.tag.Equals(targetTag))
                {
                    return collider.gameObject;
                }
            }
            
        }

        return null;
    }

}
