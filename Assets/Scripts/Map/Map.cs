using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Pathfinding;

public class Map : MonoBehaviour
{
    public Tilemap Walls;
    public Tile PlaceableTile;
    public AstarPath AStar;

    public GameObject obj;

    public void ChangePlaceableTile(Tile t) {
        PlaceableTile = t;
    }

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        DetectPlacement(); // Run logic to detect possible placement of a Tile by the user
    }

    IEnumerator refreshHitbox(int secs)
    {
        yield return new WaitForSeconds(secs);
        AstarPath.active.Scan();
        Debug.Log("refreshing");
    }

    // Detects clicks to place something on the map
    private void DetectPlacement()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Convert click position to TileMap position
            var worldPoint = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var tpos = Walls.WorldToCell(worldPoint);

            // Try to get a tile from cell position to check if we can place here
            var tile = Walls.GetTile(tpos);

            if(!tile)
            {
                var newObj = GameObject.Instantiate(obj);
                newObj.transform.position = Walls.GetCellCenterWorld(tpos);
                PlaceableTile.gameObject = newObj;

                Walls.SetTile(tpos, PlaceableTile);

                // Rerun scan for AStar paths following new object placement
                StartCoroutine(refreshHitbox(1));
            }
        }
    }
}
