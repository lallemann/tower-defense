using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDropShop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] private Canvas canvas;

    public GameObject TowerPrefab;
    public GameObject panel;
    private GameObject _newTower;
    public void OnBeginDrag(PointerEventData eventData)
    {
        _newTower = Object.Instantiate(TowerPrefab);
        Vector3 pos = Camera.main.ViewportToWorldPoint(panel.transform.localPosition + this.transform.localPosition);
        Debug.Log("Position panel : " + panel.transform.localPosition);
        Debug.Log("Position image :" + this.transform.localPosition);
        _newTower.transform.position = Camera.main.WorldToViewportPoint(pos);
    }

    public void OnDrag(PointerEventData eventData)
    {
        _newTower.transform.position += (Vector3)eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
