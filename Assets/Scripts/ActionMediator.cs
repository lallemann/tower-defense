using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMediator : MonoBehaviour, IMediator
{

    public GameControl GameController;

    private static ActionMediator _Instance;

    public static ActionMediator Instance
    {
        get
        {
            if (_Instance??false || _Instance == null)
            {
                _Instance = FindObjectOfType<ActionMediator>();
            }

            return _Instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Notify(GameObject sender, string message)
    {
        if ("Nexus".Equals(sender.tag) && "Death".Equals(message))
        {
            GameController.Lose();
        }
    }
}
