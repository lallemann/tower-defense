using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    
    public GameObject TowerPrefab;
    public  GameObject _tower1;
    
    // Start is called before the first frame update
    void Start()
    {
        // _tower1 = Object.Instantiate(TowerPrefab);
        TowerAttack tower1Attack = _tower1.GetComponent(typeof (TowerAttack)) as TowerAttack;

        // tower1Attack.RegisterTowerModifier(new FireTowerModifier(tower1Attack, _tower1));
        tower1Attack.RegisterTowerModifier(new HomingTowerModifier(tower1Attack, _tower1));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClickAddFire()
    {
        TowerAttack tower1Attack = _tower1.GetComponent(typeof (TowerAttack)) as TowerAttack;
        tower1Attack.RegisterTowerModifier(new FireTowerModifier(tower1Attack, _tower1));
    }

    public void OnClickAddWater()
    {
        TowerAttack tower1Attack = _tower1.GetComponent(typeof (TowerAttack)) as TowerAttack;
        tower1Attack.RegisterTowerModifier(new WaterTowerModifier(tower1Attack, _tower1));
    }

    public void OnClickAddHoming()
    {
        TowerAttack tower1Attack = _tower1.GetComponent(typeof (TowerAttack)) as TowerAttack;
        tower1Attack.RegisterTowerModifier(new HomingTowerModifier(tower1Attack, _tower1));
    }
}
