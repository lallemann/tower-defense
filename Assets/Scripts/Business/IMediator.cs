using UnityEngine;

public interface IMediator
{
    void Notify(GameObject sender, string message);
}
