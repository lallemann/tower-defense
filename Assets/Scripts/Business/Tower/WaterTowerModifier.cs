using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTowerModifier : TowerModifier
{
    public static GameObject AttackPrefab;
    private ITower _Wrappee;
    private GameObject _TowerObject;

    public GameObject Target;
    
    public WaterTowerModifier(ITower source, GameObject sourceObject) : base (source, sourceObject)
    {
        _Wrappee = source;
        _TowerObject = sourceObject;
        AttackPrefab = Resources.Load("Prefabs/WaterAttack") as GameObject;
    }

    public override void SetTarget(GameObject target)
    {
        this.Target = target;
    }
    
    public override void Shoot()
    {
        GameObject waterBullet = Object.Instantiate(AttackPrefab, _TowerObject.transform.position, _TowerObject.transform.rotation);
        // waterBullet.GetComponent<WaterJet>().Target = this.Target;
    }
}
