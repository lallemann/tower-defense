using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class FireTowerModifier : TowerModifier
{
    public GameObject AttackPrefab;
    public GameObject Target;
    private ITower _Wrappee;
    private GameObject _TowerObject;

    public FireTowerModifier(ITower source, GameObject sourceObject) : base (source, sourceObject)
    {
        _Wrappee = source;    
        _TowerObject = sourceObject;
        AttackPrefab = Resources.Load("Prefabs/Fireball") as GameObject;
    }

    public override void SetTarget(GameObject target)
    {
        this.Target = target;
    }
    
    public override void Shoot()
    {
        GameObject fireBullet = Object.Instantiate(AttackPrefab, _TowerObject.transform.position, _TowerObject.transform.rotation);
        fireBullet.GetComponent<Fireball>().Target = this.Target;
    }
}
