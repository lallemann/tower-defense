using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingTowerModifier : TowerModifier
{
    public static GameObject AttackPrefab;
    public GameObject Target;
    private ITower _Wrappee;
    private GameObject _TowerObject;

    public HomingTowerModifier(ITower source, GameObject sourceObject) : base (source, sourceObject)
    {
        _Wrappee = source;    
        _TowerObject = sourceObject;
        AttackPrefab = Resources.Load("Prefabs/HomingStar") as GameObject;
    }

    public override void SetTarget(GameObject target)
    {
        this.Target = target;
    }
    
    public override void Shoot()
    {
        GameObject star = Object.Instantiate(AttackPrefab, _TowerObject.transform.position, _TowerObject.transform.rotation);
        star.GetComponent<ShootingStar>().Target = this.Target;
    }
}
