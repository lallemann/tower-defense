using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TowerModifier : ITower
{
    private ITower _Wrappee;
    private GameObject _TowerObject;
    
    public TowerModifier(ITower source, GameObject sourceObject)
    {
        
    }

    public abstract void SetTarget(GameObject target);
    
    public abstract void Shoot();
}
