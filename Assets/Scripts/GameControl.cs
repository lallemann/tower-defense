using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{

    public GameObject SpawnController;
    private SpawnControl _SpawnControl;
    public int GoalWaveNumber = 10;
    public float WaveSizeIncreaseMultiplier = 1.20f;

    // Start is called before the first frame update
    void Start()
    {
        _SpawnControl = SpawnController.GetComponent<SpawnControl>();
        _SpawnControl.StartWave();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_SpawnControl.IsWaveLoading && _SpawnControl.HasWaveEnded())
        {
            if (_SpawnControl.WaveNumber == GoalWaveNumber)
            {
                Win();
            }
            else
            {
                IncreaseWaveSize();
                _SpawnControl.StartWave();
            }
        }
    }

    void Win()
    {
        Debug.Log("c'est gagné");
    }

    public void Lose()
    {
        Debug.Log("Mort du Nexus !");   
        // Application.Exit();
    }

    void IncreaseWaveSize()
    {
        Debug.Log("avant : _SpawnControl.WaveSize: "+_SpawnControl.WaveSize);
        _SpawnControl.WaveSize = (int)((float)_SpawnControl.WaveSize * WaveSizeIncreaseMultiplier);
        Debug.Log("après : _SpawnControl.WaveSize: "+_SpawnControl.WaveSize);

        _SpawnControl.IncreaseMonstersLevel();
    }
}
