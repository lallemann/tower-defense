using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterHealth : MonoBehaviour, IDamageable
{
    public int Health = 100;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecieveDamage(int damage)
    {
        // Debug.Log(gameObject+" reçu "+damage+" dmg.");

        this.Health = this.Health - damage;

        if (this.Health < 0)
        {
            Die();
        }
    }

    private void Die()
    {
        GameObject.Destroy(gameObject, 0);
    }
}
