using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAttack : MonsterAttack
{
    public GameObject Target;
    public float AttackCooldown = 0.5f;
    private GameObject _AttackPrefab;
    public float FireballSpeed = 10f;
    public float FireballSpeedMultiplier = 1.25f;
    public int FireballExplPower = 30;
    public float FireballExplPowerMultiplier = 1.4f;
    public float AttackRange = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Range = AttackRange;
        _ListOfTargets.Add("Nexus");
        _AttackPrefab = Resources.Load("Prefabs/Fireball") as GameObject;
        StartCoroutine("ShootRepeatedlyCoroutine");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ShootRepeatedlyCoroutine()
    {
        for(;;)
        {
            if (_Target??false && IsInRangeFromTarget())
            {
                Attack();
            }
            else 
            {
                SearchNewTarget();
            }
            yield return new WaitForSeconds(AttackCooldown);
        }
    }

    override protected void Attack()
    {
        GameObject fireBullet = Object.Instantiate(_AttackPrefab, transform.position, transform.rotation);
        fireBullet.GetComponent<Fireball>().Target = this._Target;
        fireBullet.GetComponent<Fireball>().Speed = this.FireballSpeed;
        fireBullet.GetComponent<Fireball>().ExplosionPower = this.FireballExplPower;
    }

    public void IncreasePowerLevel()
    {
        FireballSpeed = (int)((float)FireballSpeed * FireballSpeedMultiplier);
        FireballExplPower = (int)((float)FireballExplPower * FireballExplPowerMultiplier);
    }

    bool IsInRangeFromTarget()
    {
        return Target??false && Vector3.Distance(transform.position, Target.transform.position) < AttackRange;
    }
}
