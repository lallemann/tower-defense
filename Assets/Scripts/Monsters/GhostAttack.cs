using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAttack : MonsterAttack, IPowerLevel
{
    public int Power = 40;
    public float AttackCooldown = 0.3f;
    public int AttackRange = 2;
    public float PowerUpMultiplier = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        _ListOfTargets.Add("Nexus");
        StartCoroutine("AttackCoroutine");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator AttackCoroutine()
    {
        for(;;)
        {
            if (IsInAttackRangeFromTarget())
            {
                Attack();
            }
            else 
            {
                SearchNewTarget();
            }
            yield return new WaitForSeconds(AttackCooldown);
        }
    }

    override protected void Attack()
    {
        CombatMediator.Instance.DealDamage(
                    _Target.GetComponent<IDamageable>(),
                    Power
                );
    }

    private bool IsInAttackRangeFromTarget()
    {
        return (_Target??false) && 
        (Vector3.Distance(transform.position, _Target.transform.position) < AttackRange);
    }

    public void IncreasePowerLevel()
    {
        Power = (int)((float)Power * PowerUpMultiplier);
    }
}
