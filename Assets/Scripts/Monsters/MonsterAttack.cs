using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonsterAttack : MonoBehaviour
{

    protected GameObject _Target;
    
    protected List<string> _ListOfTargets = new List<string>();

    protected float Range = 5f;

    protected void SearchNewTarget()
    {
        RadiusScannerTool rsTool = new RadiusScannerTool(gameObject, Range);
        rsTool.Targets.AddRange(_ListOfTargets);
        
        _Target = rsTool.FirstTargetWithinRadius();
    }

    abstract protected void Attack();
}
