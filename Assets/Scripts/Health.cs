using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Text healthtText;
    public Image healthBar;

    int health, maxHealth = 100;
    float lerpSpeed;
    // Start is called before the first frame update
    void Start()
    {
        healthtText.text = "Health : " + HealthPercent() + " %";
    }

    // Update is called once per frame
    void Update()
    {
        healthtText.text = "Health : " + HealthPercent() + " %";
        if (health > maxHealth) {
            health = maxHealth;
        }

        lerpSpeed = 3f * Time.deltaTime;
        HealthBarFiller();
        ColorChanger();
    }

    void HealthBarFiller()
    {
        healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, HealthDecimal(), lerpSpeed);
    }

    void ColorChanger()
    {
        Color healthColor = Color.Lerp(Color.red, Color.green, HealthDecimal());
        healthBar.color = healthColor;
    }

    public void Damage(int damagePoints)
    {
        if(health > 0) {
            health -= damagePoints;
        }
    }

    public void Heal(int healingPoints)
    {
        if (health < maxHealth) {
            health += healingPoints;
        }
    }

    public int HealthPercent()
    {
        return (int) (((decimal)health / (decimal)maxHealth)*100);
    }

    public float HealthDecimal()
    {
        return (float) ((decimal)health / (decimal)maxHealth);
    }

    public void SetStartingHealth(int startingHealth)
    {
        health = startingHealth;
        maxHealth = startingHealth;
    }
}
