using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class SpawnControl : MonoBehaviour
{
    private List<GameObject> _MonstersList = new List<GameObject>();
    public GameObject DragonPrefab;
    public GameObject GhostPrefab;
    public GameObject SpawnPoint;
    public int WaveSize = 5;
    public int WaveNumber {get; private set;} = 0;
    private List<GameObject> _AvailablePrefabsList = new List<GameObject>();
    public bool IsWaveLoading {get; private set;} = true;
    public GameObject Nexus;
    public float TimeBetweenSpawns = 0.3f;

    void Awake()
    {
        LoadPrefabs();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LoadPrefabs()
    {
        Debug.Log("LoadPrefabs");
        if (DragonPrefab != null)
        {
            _AvailablePrefabsList.Add(DragonPrefab);
        }

        if (GhostPrefab != null)
        {
            _AvailablePrefabsList.Add(GhostPrefab);
        }
    }

    public void StartWave()
    {
        IsWaveLoading = true; 
        WaveNumber ++;

        StartCoroutine("SpawnWaveCoroutine");

        IsWaveLoading = false;
    }

    IEnumerator SpawnWaveCoroutine()
    {
        for(int i = 0; i <= WaveSize; i++)
        {
            SpawnRandomMonster();
            yield return new WaitForSeconds(TimeBetweenSpawns);
        }
    }

    public bool HasWaveEnded()
    {
        return _MonstersList.Count == 0;
    }

    public void SpawnRandomMonster()
    {
        GameObject newMonster = Object.Instantiate(
            _AvailablePrefabsList[ Random.Range(0, _AvailablePrefabsList.Count) ],
            SpawnPoint.transform.position,
            Quaternion.identity
            );
        newMonster.GetComponent<AIDestinationSetter>().target = Nexus.transform;
        _MonstersList.Add(newMonster);
    }

    public void IncreaseMonstersLevel()
    {
        foreach (GameObject monster in _MonstersList)
        {
            monster.GetComponent<IPowerLevel>().IncreasePowerLevel();
        }
    }
}
