using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class ShopButton : MonoBehaviour
{
    public Button Button;
    public Tile NewTile;
    public Tilemap Walls;

    private bool state = false;
    public void ADbutton()
    {
        if(state)
        {
            Color newColor = Color.green;
            ColorBlock colors = ColorBlock.defaultColorBlock;
            colors.normalColor = newColor;
            Button.colors = colors;
            Button.GetComponentInChildren<Text>().text = "Selectionner";
            state = false;
            Walls.GetComponent<Map>().ChangePlaceableTile(null);
        }
        else
        {
            Color newColor = Color.red;
            ColorBlock colors = ColorBlock.defaultColorBlock;
            colors.normalColor = newColor;
            Button.colors = colors;
            Button.GetComponentInChildren<Text>().text = "Annuler";
            state = true;
            Walls.GetComponent<Map>().ChangePlaceableTile(NewTile);
        }
    }
}
